package com.omexit.blackjack.util;

import com.omexit.blackjack.domain.Card;
import com.omexit.blackjack.domain.Player;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;

public class CardUtil {

    private final static Logger logger = Logger.getLogger(CardUtil.class);
    public static HashMap<String, Integer> CARD_POINTS = new HashMap<String, Integer>();

    static {
        CARD_POINTS.put("2", 2);
        CARD_POINTS.put("3", 3);
        CARD_POINTS.put("4", 4);
        CARD_POINTS.put("5", 5);
        CARD_POINTS.put("6", 6);
        CARD_POINTS.put("7", 7);
        CARD_POINTS.put("8", 8);
        CARD_POINTS.put("9", 9);
        CARD_POINTS.put("10", 10);
        CARD_POINTS.put("J", 10);
        CARD_POINTS.put("Q", 10);
        CARD_POINTS.put("K", 10);
        CARD_POINTS.put("A", 11);
    }

    public static Card getCard(String cardName) {
        String cardNumber = cardName.substring(0, cardName.length() - 1);
        String cardSuit = cardName.substring(cardName.length() - 1);

        Card card = new Card(cardNumber, cardSuit, CARD_POINTS.get(cardNumber));
        logger.info(String.format("getCard|cardNumber=%s,cardSuit=%s, card=%s", cardNumber, cardSuit, card));

        return card;
    }

    public static int getTotalPoints(ArrayList<Card> cards) {
        int totalPoints = 0;

        for (int i = 0; i < cards.size(); i++) {
            Card card = cards.get(i);
            totalPoints += card.getCardValue();
        }

        logger.info(String.format("getTotalPoints|cards=%s, totalPoints=%s", cards, totalPoints));

        return totalPoints;
    }

    public static Player createPlayer(ArrayList<String> playerACardNames, String name) {
        Player player = new Player();
        ArrayList<Card> cards = new ArrayList<>();
        for (String cardName : playerACardNames) {
            cards.add(CardUtil.getCard(cardName));
        }
        player.setName(name);
        player.setCards(cards);
        return player;
    }

}
