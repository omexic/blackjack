package com.omexit.blackjack.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.omexit.blackjack.domain.GameResult;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;

public class FileUtil {
    private final static Logger logger = Logger.getLogger(FileUtil.class);

    public static void downloadTestCases(String fileUrl, String outputFileName) throws IOException {
        logger.info(String.format("event=DOWNLOAD_FILE_START, url=%s", fileUrl));
        long start = System.currentTimeMillis();
        FileUtils.copyURLToFile(
                new URL(fileUrl),
                new File(outputFileName),
                3000,
                60000);
        logger.info(String.format("event=DOWNLOAD_FILE_COMPLETE, url=%s, timeUsed=%s ms", fileUrl, (System.currentTimeMillis() - start)));
    }


    public static List<GameResult> readJSONFile(String outputFileName) throws FileNotFoundException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(outputFileName));

        Gson gson = new Gson();
        Type type = new TypeToken<List<GameResult>>(){}.getType();
        List<GameResult> gameResults = gson.fromJson(bufferedReader,type);
        logger.info(String.format("FileContent=%s", gameResults.toString()));
        return gameResults;
    }
}
