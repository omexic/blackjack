package com.omexit.blackjack;


import com.omexit.blackjack.domain.Card;
import com.omexit.blackjack.domain.GameResult;
import com.omexit.blackjack.domain.Player;
import com.omexit.blackjack.service.GameService;
import com.omexit.blackjack.util.CardUtil;
import com.omexit.blackjack.util.FileUtil;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class App {

    final static Logger logger = Logger.getLogger(App.class);

    private static final String FILE_URL = "https://s3-eu-west-1.amazonaws.com/yoco-testing/tests.json";
    private static final String FILE_NAME = "/home/aomeri/dev/interview/testapp/input/tests.json";

    public static void main(String[] args) {
        logger.info("Starting ....");
        try {
            FileUtil.downloadTestCases(FILE_URL, FILE_NAME);
            List<GameResult> gameResults = FileUtil.readJSONFile(FILE_NAME);

            int gameNo = 1;
            StringBuilder winners = new StringBuilder();
            for (GameResult gameResult : gameResults) {
                try {
                    GameService gameService = new GameService();
                    Player winner = gameService.getWinner(CardUtil.createPlayer(gameResult.getPlayerA(), "PlayerA"), CardUtil.createPlayer(gameResult.getPlayerB(), "PlayerB"));
                    winners.append(String.format("Game: %s, Winner: %s, MatchesExpectedResult: %s", gameNo, winner, winner.getName().equals("PlayerA") && gameResult.getPlayerAWins() ? "MatchExpectedResult" : "DoesNotMatchExpectedResult")).append("\n");
                }catch (Exception e){

                }
                gameNo++;
            }

            System.out.println();
            System.out.println();
            System.out.println("==================================================================================================================================================");
            System.out.println("WINNERS");
            System.out.println("==================================================================================================================================================");
            System.out.println(winners.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
