package com.omexit.blackjack.service;

import com.omexit.blackjack.domain.Card;
import com.omexit.blackjack.domain.Player;
import com.omexit.blackjack.util.CardUtil;

import java.util.ArrayList;

public class GameService {

    public Player getWinner(Player playerA, Player playerB) {
        Player winner = null;
        int totalPointsPlayerA = CardUtil.getTotalPoints(playerA.getCards());
        int totalPointsPlayerB = CardUtil.getTotalPoints(playerB.getCards());

        boolean playerAGreaterthan21 = totalPointsPlayerA > 21;
        boolean playerBGreaterthan21 = totalPointsPlayerB > 21;

        winner = (!playerAGreaterthan21 && playerBGreaterthan21) ? playerA :
                (playerAGreaterthan21 && !playerBGreaterthan21) ? playerB :
                        !playerAGreaterthan21 && (totalPointsPlayerA > totalPointsPlayerB) ?
                                playerA : (!playerBGreaterthan21 && (totalPointsPlayerB > totalPointsPlayerA) ? playerB : null);

        if (winner == null && (totalPointsPlayerA == totalPointsPlayerB)) {
            winner = getHighestPlayerCard(playerA, playerB);
        }


        return winner;
    }

    public static Player getHighestPlayerCard(Player playerA, Player playerB) {
        ArrayList<Card> playerACards = playerA.getCards();
        ArrayList<Card> playerBCards = playerB.getCards();
        int lowestCardNumber = Math.min(playerACards.size(), playerBCards.size());
        Player playerWithMaxNumOfCard = playerACards.size() == playerBCards.size() ? null : (playerACards.size() > lowestCardNumber ? playerA : playerB);

        Player highestPlayerCard = null;
        for (int i = 0; i < lowestCardNumber; i++) {
            if (i < lowestCardNumber) {
                if (playerACards.get(i).getCardValue() == playerBCards.get(i).getCardValue()) {
                    continue;
                } else {
                    if (playerACards.get(i).getCardValue() > playerBCards.get(i).getCardValue()) {
                        highestPlayerCard = playerA;
                        break;
                    } else {
                        highestPlayerCard = playerB;
                        break;
                    }
                }
            } else {
                return playerWithMaxNumOfCard;
            }
        }
        return highestPlayerCard;
    }
}
