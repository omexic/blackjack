
package com.omexit.blackjack.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.ArrayList;

public class GameResult implements Serializable {

    @SerializedName("playerA")
    @Expose
    private ArrayList<String> playerA = new ArrayList<String>();
    @SerializedName("playerAWins")
    @Expose
    private Boolean playerAWins;
    @SerializedName("playerB")
    @Expose
    private ArrayList<String> playerB = new ArrayList<String>();
    private final static long serialVersionUID = -6357530459880977553L;

    /**
     * No args constructor for use in serialization
     */
    public GameResult() {
    }

    /**
     * @param playerAWins
     * @param playerA
     * @param playerB
     */
    public GameResult(ArrayList<String> playerA, Boolean playerAWins, ArrayList<String> playerB) {
        super();
        this.playerA = playerA;
        this.playerAWins = playerAWins;
        this.playerB = playerB;
    }

    public ArrayList<String> getPlayerA() {
        return playerA;
    }

    public void setPlayerA(ArrayList<String> playerA) {
        this.playerA = playerA;
    }

    public Boolean getPlayerAWins() {
        return playerAWins;
    }

    public void setPlayerAWins(Boolean playerAWins) {
        this.playerAWins = playerAWins;
    }

    public ArrayList<String> getPlayerB() {
        return playerB;
    }

    public void setPlayerB(ArrayList<String> playerB) {
        this.playerB = playerB;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("playerA", playerA).append("playerAWins", playerAWins).append("playerB", playerB).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(playerAWins).append(playerA).append(playerB).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GameResult) == false) {
            return false;
        }
        GameResult rhs = ((GameResult) other);
        return new EqualsBuilder().append(playerAWins, rhs.playerAWins).append(playerA, rhs.playerA).append(playerB, rhs.playerB).isEquals();
    }

}
