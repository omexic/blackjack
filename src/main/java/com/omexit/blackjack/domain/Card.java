package com.omexit.blackjack.domain;

public class Card {
    private String cardNumber;
    private String cardSuit;
    private int cardValue;

    public Card(String cardNumber, String cardSuit, int cardValue) {
        this.cardNumber = cardNumber;
        this.cardSuit = cardSuit;
        this.cardValue = cardValue;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardSuit() {
        return cardSuit;
    }

    public void setCardSuit(String cardSuit) {
        this.cardSuit = cardSuit;
    }

    public int getCardValue() {
        return cardValue;
    }

    public void setCardValue(int cardValue) {
        this.cardValue = cardValue;
    }

    @Override
    public String toString() {
        return "Card{" +
                "cardNumber='" + cardNumber + '\'' +
                ", cardSuit='" + cardSuit + '\'' +
                ", cardValue=" + cardValue +
                '}';
    }
}
