package com.omexit.testapp.util;

import com.omexit.blackjack.domain.Card;
import com.omexit.blackjack.util.CardUtil;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class CardUtilTest {

    @Test
    public void testGetCard() {
        String cardName = "5D";
        Card expectedCard = new Card("5", "D", 5);

        Card card = CardUtil.getCard(cardName);
        assertEquals(expectedCard.getCardValue(), card.getCardValue());
        assertEquals(expectedCard.getCardNumber(), card.getCardNumber());
        assertEquals(expectedCard.getCardSuit(), card.getCardSuit());
    }

    @Test
    public void testGetTotalPoints() {
        ArrayList<Card> cards = new ArrayList<>();
        Card card1 = new Card("5", "D", 5);
        Card card2 = new Card("9", "S", 9);
        Card card3 = new Card("A", "H", 11);
        Card card4 = new Card("J", "C", 10);
        cards.add(card1);
        cards.add(card2);
        cards.add(card3);
        cards.add(card4);

        assertEquals(35, CardUtil.getTotalPoints(cards));
    }
}
