package com.omexit.testapp.service;

import com.omexit.blackjack.domain.Player;
import com.omexit.blackjack.service.GameService;
import com.omexit.blackjack.util.CardUtil;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GameServiceTest {

    @Test
    public void testWinnerWithHighestPlayerACard() {
        GameService gameService = new GameService();

        Player playerA = CardUtil.createPlayer(new ArrayList<>(Arrays.asList("8D", "JS")), "PlayerA");
        Player playerB = CardUtil.createPlayer(new ArrayList<>(Arrays.asList("5D", "QS", "3H")), "PlayerB");

        Player winner = gameService.getWinner(playerA, playerB);
        assertEquals("PlayerA", winner.getName());
    }

    @Test
    public void testWinnerWhenPlayerBCardIsGreaterThan21() {
        GameService gameService = new GameService();

        Player playerA = CardUtil.createPlayer(new ArrayList<>(Arrays.asList("10D", "JH")), "PlayerA");
        Player playerB = CardUtil.createPlayer(new ArrayList<>(Arrays.asList("QS", "4C", "JC")), "PlayerB");

        Player winner = gameService.getWinner(playerA, playerB);
        assertEquals("PlayerA", winner.getName());
    }

    @Test
    public void testWinnerWhenPlayerACardIsGreaterThan21() {
        GameService gameService = new GameService();

        Player playerA = CardUtil.createPlayer(new ArrayList<>(Arrays.asList("JH", "KS", "3C")), "PlayerA");
        Player playerB = CardUtil.createPlayer(new ArrayList<>(Arrays.asList("JC", "QS")), "PlayerB");

        Player winner = gameService.getWinner(playerA, playerB);
        assertEquals("PlayerB", winner.getName());
    }
}
